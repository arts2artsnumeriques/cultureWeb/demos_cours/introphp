<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
?>

  <?php
    $hello = 'hello world';
  ?>
  <h1><?php echo $hello; ?></h1>

  <?php include('head.php'); ?>
  <?php include('nav.php'); ?>

  <main>
    <section>
      <h2>section</h2>
      <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Amet, omnis. Maxime hic itaque harum alias ratione fugiat magni voluptatem aliquid architecto. Maxime nostrum accusantium natus quasi laborum iusto at error?</p>
    </section>
    <section>
      <h2>section</h2>
      <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Amet, omnis. Maxime hic itaque harum alias ratione fugiat magni voluptatem aliquid architecto. Maxime nostrum accusantium natus quasi laborum iusto at error?</p>
    </section>
  </main>

  <?php include('footer.php') ?>
  <?php include('foot.php') ?>
